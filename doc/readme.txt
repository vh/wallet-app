
1. create schema
    CREATE SCHEMA `wallet_db` DEFAULT CHARACTER SET utf8 ;

2. create user
    create user 'appuser'@'localhost' identified by 'apppass';

3. grant all (the first run only to generate necessary structure)
    grant all on wallet_db.* to 'appuser'@'localhost';

4. execute maven command to build project and generate java code using wallet.proto (the first run only, later we can run test too)
    mvn -DskipTests=true clean package

5. run server (check in server console "gRPC Server started, listening on port 6565" and corresponding tables are created)
    java -jar wallet-app/wallet-server/target/wallet-server-1.0-SNAPSHOT.jar

6. stop the server
    ctr + c

7. grant minimum privileges
    grant select, insert, delete, update on wallet_db.* to 'appuser'@'localhost';

8. insert test data into mysql

    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (11, 10000, 1, 1);
    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (12, 10000, 2, 1);
    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (13, 10000, 3, 1);

    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (21, 10000, 1, 2);
    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (22, 10000, 2, 2);
    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (23, 10000, 3, 2);

    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (31, 10000, 1, 3);
    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (32, 10000, 2, 3);
    INSERT INTO wallet_db.account (id, amount, currency_id, owner_id) VALUES (33, 10000, 3, 3);


    INSERT INTO wallet_db.dic_currency (id, code) VALUES (1, 'EUR');
    INSERT INTO wallet_db.dic_currency (id, code) VALUES (2, 'USD');
    INSERT INTO wallet_db.dic_currency (id, code) VALUES (3, 'GBP');
    INSERT INTO wallet_db.dic_currency (id, code) VALUES (4, 'AMD');

    COMMIT

9. update application.properties line with key "spring.jpa.hibernate.ddl-auto"
    from "create" to "none"

10. execute maven command to rebuild project
        mvn clean package

11. run wallet-server
    java -jar wallet-server/target/wallet-server-1.0-SNAPSHOT.jar

12. run wallet-client
    java -jar wallet-client/target/wallet-client-1.0-SNAPSHOT.jar 3 2000 10

===============================
== THE RESULT ON MY COMPUTER ==
===============================

user - 3
thread per user - 2000
rounds per thread - 10

More than this values cause OutOfMemory

Core™ i5-6600 CPU @ 3.30GHz × 4
RAM 16GB

Duration 15 sec
Call total 399715
Call per second 26647 RPCs/s