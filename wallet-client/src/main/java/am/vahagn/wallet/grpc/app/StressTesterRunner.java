package am.vahagn.wallet.grpc.app;

import am.vahagn.wallet.grpc.app.gen.Wallet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@SpringBootApplication
public class StressTesterRunner implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(StressTesterRunner.class);
    private static Random random = new Random();

    private List<Round> rounds;
    private WalletApplicationClient client;

    private int userCount;
    private int threadsPerUser;
    private int roundsPerThread;
    private long rpcCount;

    public StressTesterRunner() {
        buildRounds();
        client = new WalletApplicationClient();
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(String.format("main(%s)", Arrays.toString(args)));
        main(args);
    }

    public static void main(String[] args) {

        try {
            StressTesterRunner testerRunner = new StressTesterRunner();
            testerRunner.userCount = Integer.valueOf(args[0]);
            testerRunner.threadsPerUser = Integer.valueOf(args[1]);
            testerRunner.roundsPerThread = Integer.valueOf(args[2]);
            testerRunner.go();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void go() throws InterruptedException {

        rpcCount = 0;
        long start = System.currentTimeMillis();

        for(int i = 0; i < userCount; i++) {

            StressTester tester = new StressTester(threadsPerUser, roundsPerThread);
            tester.stress(new Thread("Main tread") {
                public void run() {
                    try {
                        Round round = rounds.get(random.nextInt(3));
                        LOGGER.info("----------------------");
                        LOGGER.info("Round {}", round.getName());
                        LOGGER.info("----------------------");
                        List<Event> events = round.getEvents();
                        for (Event event : events) {
                            executeEvent(event);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

            tester.shutdown();
        }

        long second = (System.currentTimeMillis() - start);
        System.out.println("Duration " + second + " msc");
        System.out.println("Call total " + rpcCount);
        System.out.println("Call per second " + rpcCount / second * 1000 + " RPCs/s");
    }

    private void executeEvent(Event event) {
        rpcCount++;
        switch (event.getEventType()) {
            case DEPOSIT:
                LOGGER.info("Deposit {} {}", event.getAmount(), event.getCurrency());
//                client.deposit(event.getAmount(), event.getUserId(), event.getCurrency());
                client.depositAsync(event.getAmount(), event.getUserId(), event.getCurrency());
                break;
            case WITHDRAW:
                LOGGER.info("Withdraw {} {}", event.getAmount(), event.getCurrency());
//                client.withdraw(event.getAmount(), event.getUserId(), event.getCurrency());
                client.withdrawAsync(event.getAmount(), event.getUserId(), event.getCurrency());
                break;
            case BALANCE:
                LOGGER.info("Get Balance");
//                client.balance(event.getUserId());
                client.balanceAsync(event.getUserId());
                break;
            default:
                throw new IllegalArgumentException("Not allowed operation!");

        }
    }

    private void buildRounds() {

        rounds = new ArrayList<>();

        Round round1 = new Round("A");
        round1.addEvent(new Event(1, EventType.DEPOSIT, 100, Wallet.Currency.USD));
        round1.addEvent(new Event(1, EventType.WITHDRAW, 200, Wallet.Currency.USD));
        round1.addEvent(new Event(1, EventType.DEPOSIT, 100, Wallet.Currency.EUR));
        round1.addEvent(new Event(1, EventType.BALANCE, 0, null));
        round1.addEvent(new Event(1, EventType.WITHDRAW, 100, Wallet.Currency.USD));
        round1.addEvent(new Event(1, EventType.BALANCE, 0, null));
        round1.addEvent(new Event(1, EventType.WITHDRAW, 100, Wallet.Currency.USD));
        rounds.add(round1);

        Round round2 = new Round("B");
        round2.addEvent(new Event(2, EventType.WITHDRAW, 100, Wallet.Currency.GBP));
        round2.addEvent(new Event(2, EventType.DEPOSIT, 300, Wallet.Currency.GBP));
        round2.addEvent(new Event(2, EventType.WITHDRAW, 100, Wallet.Currency.GBP));
        round2.addEvent(new Event(2, EventType.WITHDRAW, 100, Wallet.Currency.GBP));
        round2.addEvent(new Event(2, EventType.WITHDRAW, 100, Wallet.Currency.GBP));
        rounds.add(round2);

        Round round3 = new Round("C");
        round3.addEvent(new Event(3, EventType.BALANCE, 0, null));
        round3.addEvent(new Event(3, EventType.DEPOSIT, 100, Wallet.Currency.USD));
        round3.addEvent(new Event(3, EventType.DEPOSIT, 100, Wallet.Currency.USD));
        round3.addEvent(new Event(3, EventType.WITHDRAW, 100, Wallet.Currency.USD));
        round3.addEvent(new Event(3, EventType.DEPOSIT, 100, Wallet.Currency.USD));
        round3.addEvent(new Event(3, EventType.BALANCE, 0, null));
        round3.addEvent(new Event(3, EventType.WITHDRAW, 200, Wallet.Currency.USD));
        round3.addEvent(new Event(3, EventType.BALANCE, 0, null));
        rounds.add(round3);
    }
}