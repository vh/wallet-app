package am.vahagn.wallet.grpc.app;

import am.vahagn.wallet.grpc.app.gen.WalletServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static am.vahagn.wallet.grpc.app.gen.Wallet.*;

public class WalletApplicationClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(WalletApplicationClient.class);

    private WalletServiceGrpc.WalletServiceStub asyncStub;
//    private WalletServiceGrpc.WalletServiceFutureStub stub;
    private WalletServiceGrpc.WalletServiceBlockingStub blockingStub;

    public WalletApplicationClient() {
//        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565).usePlaintext().build();
        final ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
//                .nameResolverFactory(new DnsNameResolverProvider())
//                .loadBalancerFactory(RoundRobinLoadBalancerFactory.getInstance())
                .usePlaintext()
                .build();
        asyncStub = WalletServiceGrpc.newStub(channel);
//        stub = WalletServiceGrpc.newFutureStub(channel);
        blockingStub = WalletServiceGrpc.newBlockingStub(channel);
    }

    /*public static void main(String[] args) {

        WalletApplicationClient client = new WalletApplicationClient();
//        client.deposit(123, 1, Currency.AMD);
//        client.withdraw(200, 1, Currency.EUR);
        client.balance(1);
    }*/

    public void deposit(double amount, long userId, Currency currency) {
        DepositRequest request = DepositRequest.newBuilder()
                .setAmount(amount)
                .setUserId(userId)
                .setCurrency(currency)
                .build();
//        LOGGER.info("client sending {}", request);
        DepositResponse response = blockingStub.deposit(request);
//        LOGGER.info("client received {}", response);
    }

    public void depositAsync(double amount, long userId, Currency currency) {

        DepositRequest request = DepositRequest.newBuilder()
                .setAmount(amount)
                .setUserId(userId)
                .setCurrency(currency)
                .build();

        StreamObserver<DepositResponse> response = new StreamObserver<DepositResponse>() {
            @Override
            public void onNext(DepositResponse response) {
                LOGGER.info("Deposit Async {}", response);
            }

            @Override
            public void onError(Throwable t) {
                Status status = Status.fromThrowable(t);
                LOGGER.error("Deposit Async error: {0}", status);
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Deposit Async finished.");
            }
        };
        asyncStub.deposit(request, response);
    }

    public void withdraw(double amount, long userId, Currency currency) {
        WithdrawRequest request = WithdrawRequest.newBuilder()
                .setAmount(amount)
                .setUserId(userId)
                .setCurrency(currency)
                .build();
//        LOGGER.info("client sending {}", request);
        WithdrawResponse response = blockingStub.withdraw(request);
//        LOGGER.info("client received {}", response);
    }

    public void withdrawAsync(double amount, long userId, Currency currency) {

        WithdrawRequest request = WithdrawRequest.newBuilder()
                .setAmount(amount)
                .setUserId(userId)
                .setCurrency(currency)
                .build();

        StreamObserver<WithdrawResponse> response = new StreamObserver<WithdrawResponse>() {
            @Override
            public void onNext(WithdrawResponse response) {
                LOGGER.info("Withdraw Async {}", response);
            }

            @Override
            public void onError(Throwable t) {
                Status status = Status.fromThrowable(t);
                LOGGER.error("Withdraw Async error: {0}", status);
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Withdraw Async finished.");
            }
        };

        asyncStub.withdraw(request, response);
    }

    public void balance(long userId) {
        BalanceRequest request = BalanceRequest.newBuilder()
                .setUserId(userId)
                .build();
//        LOGGER.info("client sending {}", request);
        BalanceResponse response = blockingStub.balance(request);
//        LOGGER.info("client received {}", response);
    }

    public void balanceAsync(long userId) {

        BalanceRequest request = BalanceRequest.newBuilder()
                .setUserId(userId)
                .build();

        StreamObserver<BalanceResponse> response = new StreamObserver<BalanceResponse>() {
            @Override
            public void onNext(BalanceResponse response) {
                LOGGER.info("Balance Async {}", response);
            }

            @Override
            public void onError(Throwable t) {
                Status status = Status.fromThrowable(t);
                LOGGER.error("Balance Async error: {0}", status);
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Balance Async finished.");
            }
        };

        asyncStub.balance(request, response);
    }
}