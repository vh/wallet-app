package am.vahagn.wallet.grpc.app;

import java.util.ArrayList;
import java.util.List;

public class Round {

    private String name;
    private List<Event> events;

    public Round(String name) {
        this.name = name;
        events = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addEvent(Event event) {
        events.add(event);
    }

    public List<Event> getEvents() {
        return events;
    }
}