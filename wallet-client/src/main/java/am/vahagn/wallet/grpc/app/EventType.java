package am.vahagn.wallet.grpc.app;

public enum EventType {
    DEPOSIT, WITHDRAW, BALANCE;
}