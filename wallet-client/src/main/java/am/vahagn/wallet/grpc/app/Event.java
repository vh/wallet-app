package am.vahagn.wallet.grpc.app;

import static am.vahagn.wallet.grpc.app.gen.Wallet.*;

public class Event {

    private long userId;
    private EventType eventType;
    private double amount;
    private Currency currency;

    public Event() {
    }

    public Event(long userId, EventType eventType, double amount, Currency currency) {
        this.userId = userId;
        this.eventType = eventType;
        this.amount = amount;
        this.currency = currency;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}