package am.vahagn.wallet.grpc.app;

import am.vahagn.wallet.grpc.app.dao.entity.AccountEntity;
import am.vahagn.wallet.grpc.app.dao.repository.AccountRepository;
import am.vahagn.wallet.grpc.app.gen.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountManagerImpl {

    @Autowired
    private AccountRepository accountRepository;

    boolean deposit(long ownerId, double amount, Wallet.Currency currency) {
        AccountEntity entity = accountRepository.findByOwnerIdAndCurrency(ownerId, currency);
        entity.setAmount(entity.getAmount() + amount);
        accountRepository.save(entity);
        return true;
    }

    boolean withdraw(long ownerId, double amount, Wallet.Currency currency) {
        AccountEntity entity = accountRepository.findByOwnerIdAndCurrency(ownerId, currency);
        entity.setAmount(entity.getAmount() - amount);
        accountRepository.save(entity);
        return true;
    }

    List<AccountEntity> balance(long ownerId) {
        return accountRepository.findByOwnerId(ownerId);
    }
}