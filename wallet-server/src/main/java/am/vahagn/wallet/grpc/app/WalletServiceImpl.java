package am.vahagn.wallet.grpc.app;

import am.vahagn.wallet.grpc.app.dao.entity.AccountEntity;
import am.vahagn.wallet.grpc.app.gen.Wallet;
import am.vahagn.wallet.grpc.app.gen.WalletServiceGrpc;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static am.vahagn.wallet.grpc.app.dao.util.ResponseCode.*;

@GRpcService
public class WalletServiceImpl extends WalletServiceGrpc.WalletServiceImplBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(WalletServiceImpl.class);

    @Autowired
    private AccountManagerImpl accountManager;

    @Override
    public synchronized void deposit(Wallet.DepositRequest request, StreamObserver<Wallet.DepositResponse> responseObserver) {

        long ownerId = request.getUserId();
        double amount = request.getAmount();
        Wallet.Currency currency = request.getCurrency();

        LOGGER.info("Make a deposit of {} {} to user with id {}", currency.name(), amount, ownerId);

        if (currency != Wallet.Currency.EUR
                && currency != Wallet.Currency.USD
                && currency != Wallet.Currency.GBP) {
            Wallet.DepositResponse.Builder response = Wallet.DepositResponse
                    .newBuilder()
                    .setCode(UNKNOWN_CURRENCY.getCod())
                    .setMsg(UNKNOWN_CURRENCY.getValue());

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();
            return;
        }

        boolean result = accountManager.deposit(ownerId, amount, currency);

        Wallet.DepositResponse.Builder response = Wallet.DepositResponse
                .newBuilder()
                    .setCode(result ? SUCCESS.getCod(): FAIL.getCod())
                    .setMsg(result ? SUCCESS.getValue(): FAIL.getValue());

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public synchronized void withdraw(Wallet.WithdrawRequest request, StreamObserver<Wallet.WithdrawResponse> responseObserver) {

        long ownerId = request.getUserId();
        double amount = request.getAmount();
        Wallet.Currency currency = request.getCurrency();

        LOGGER.info("Make a withdraw of {} {} for user with id {}", currency.name(), amount, ownerId);

        if (currency != Wallet.Currency.EUR
                && currency != Wallet.Currency.USD
                && currency != Wallet.Currency.GBP) {
            Wallet.WithdrawResponse.Builder response = Wallet.WithdrawResponse
                    .newBuilder()
                    .setCode(UNKNOWN_CURRENCY.getCod())
                    .setMsg(UNKNOWN_CURRENCY.getValue());

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();
            return;
        }

        List<AccountEntity> balances = accountManager.balance(ownerId);
        for (AccountEntity account : balances) {
            if (currency == account.getCurrency() && account.getAmount() < amount) {
                Wallet.WithdrawResponse.Builder response = Wallet.WithdrawResponse
                        .newBuilder()
                        .setCode(INSUFFICENT_FUNDS.getCod())
                        .setMsg(INSUFFICENT_FUNDS.getValue());

                responseObserver.onNext(response.build());
                responseObserver.onCompleted();
                return;
            }
        }

        boolean result = accountManager.withdraw(ownerId, amount, currency);

        Wallet.WithdrawResponse.Builder response = Wallet.WithdrawResponse
                .newBuilder()
                .setCode(result ? SUCCESS.getCod(): FAIL.getCod())
                .setMsg(result ? SUCCESS.getValue(): FAIL.getValue());

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public synchronized void balance(Wallet.BalanceRequest request, StreamObserver<Wallet.BalanceResponse> responseObserver) {

        long ownerId = request.getUserId();

        LOGGER.info("Check balance for user with id {}", ownerId);

        Map<String, Double> map = new HashMap<>();
        List<AccountEntity> balances = accountManager.balance(ownerId);
        for (AccountEntity balance : balances) {
            map.put(balance.getCurrency().name(), balance.getAmount());
        }

        Wallet.BalanceResponse.Builder response = Wallet.BalanceResponse
                .newBuilder()
                .setCode(balances.size() == 3 ? SUCCESS.getCod(): FAIL.getCod())
                .setMsg(balances.size() == 3 ? SUCCESS.getValue(): FAIL.getValue())
                .putAllBalances(map);

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }
}