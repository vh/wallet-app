package am.vahagn.wallet.grpc.app.dao.entity;

import am.vahagn.wallet.grpc.app.gen.Wallet;

import javax.persistence.*;

@Entity
@Table(name = "account", uniqueConstraints={
        @UniqueConstraint(columnNames={"owner_id", "currency_id"})
})
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "owner_id", nullable = false)
    private Long ownerId;

    @Column(name = "currency_id", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Wallet.Currency currency;

    @Column(name = "amount", nullable = false)
    private Double amount;

    public AccountEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Wallet.Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Wallet.Currency currency) {
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}