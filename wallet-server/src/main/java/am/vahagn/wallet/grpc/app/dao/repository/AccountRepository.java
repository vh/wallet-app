package am.vahagn.wallet.grpc.app.dao.repository;

import am.vahagn.wallet.grpc.app.dao.entity.AccountEntity;
import am.vahagn.wallet.grpc.app.gen.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {

    List<AccountEntity> findByOwnerId(long ownerId);

    AccountEntity findByOwnerIdAndCurrency(long ownerId, Wallet.Currency currency);
}