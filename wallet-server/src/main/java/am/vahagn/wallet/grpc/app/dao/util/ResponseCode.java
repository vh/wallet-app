package am.vahagn.wallet.grpc.app.dao.util;

public enum ResponseCode {

    SUCCESS             (0, "Success"),
    FAIL                (1, "FAil"),
    UNKNOWN_CURRENCY    (2, "Unknown currency"),
    INSUFFICENT_FUNDS   (3, "Insufficient Funds");

    private int cod;
    private String value;

    ResponseCode(int cod, String value) {
        this.cod = cod;
        this.value = value;
    }

    public int getCod() {
        return cod;
    }

    public String getValue() {
        return value;
    }
}