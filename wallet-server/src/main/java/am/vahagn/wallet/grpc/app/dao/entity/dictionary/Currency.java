package am.vahagn.wallet.grpc.app.dao.entity.dictionary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dic_currency")
public class Currency {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "code")
    private String code;

    public Currency() {
    }

    public Currency(Integer id, String code) {
        this.id = id;
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}