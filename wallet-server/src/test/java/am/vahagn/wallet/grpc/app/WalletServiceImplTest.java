package am.vahagn.wallet.grpc.app;

import am.vahagn.wallet.grpc.app.dao.util.ResponseCode;
import am.vahagn.wallet.grpc.app.gen.Wallet;
import io.grpc.stub.StreamObserver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WalletServiceImplTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WalletServiceImplTest.class);

    @Autowired
    private WalletServiceImpl walletService;

    private long userId;

    @Before
    public void setup() {
        userId = 1;
    }

    @Test
    public void deposit() {

        Wallet.DepositRequest request = Wallet.DepositRequest.newBuilder()
                .setAmount(1)
                .setUserId(userId)
                .setCurrency(Wallet.Currency.EUR)
                .build();
        walletService.deposit(request, new StreamObserver<Wallet.DepositResponse>() {
            @Override
            public void onNext(Wallet.DepositResponse depositResponse) {
                LOGGER.info("DepositResponse onNext is {}", depositResponse);
                Assert.assertEquals(depositResponse.getCode(), ResponseCode.SUCCESS.getCod());
            }

            @Override
            public void onError(Throwable throwable) {
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Deposit Async test finished.");
            }
        });
    }

    @Test
    public void withdraw() {

        Wallet.WithdrawRequest request = Wallet.WithdrawRequest.newBuilder()
                .setAmount(1)
                .setUserId(userId)
                .setCurrency(Wallet.Currency.EUR)
                .build();
        walletService.withdraw(request, new StreamObserver<Wallet.WithdrawResponse>() {
            @Override
            public void onNext(Wallet.WithdrawResponse withdrawResponse) {
                LOGGER.info("WithdrawResponse onNext is {}", withdrawResponse);
                Assert.assertEquals(withdrawResponse.getCode(), ResponseCode.SUCCESS.getCod());
            }

            @Override
            public void onError(Throwable throwable) {
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Withdraw Async test finished.");
            }
        });
    }

    @Test
    public void balance() {

        Wallet.BalanceRequest request = Wallet.BalanceRequest.newBuilder()
                .setUserId(userId)
                .build();

        walletService.balance(request, new StreamObserver<Wallet.BalanceResponse>() {
            @Override
            public void onNext(Wallet.BalanceResponse balanceResponse) {
                LOGGER.info("BalanceResponse onNext is {}", balanceResponse);
                Assert.assertEquals(balanceResponse.getCode(), ResponseCode.SUCCESS.getCod());
            }

            @Override
            public void onError(Throwable throwable) {
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Balance Async test finished.");
            }
        });
    }
}